//
//  WordDetails.swift
//  BGL
//
//  Created by MAC on 1/28/17.
//  Copyright © 2017 Zahur Ghotlawala. All rights reserved.
//

import Foundation


class WordDetails {
    var comparator : String = "";
    var counter : Int = 0;
    var isPrime : Bool = false;
    
    init(comparator : String) {
        self.comparator = comparator;
        self.counter = 1;
    }

    func checkForPrime(){
        
        isPrime = true;
        if counter == 1 {
            isPrime = false
        }
        var i = 2
        while i < counter {
            if counter % i == 0 {
                isPrime = false
            }
            i += 1
        }
    
    }
    
    func print(){
        NSLog("Word: \(comparator) Counts: \(counter) isPrime:\(isPrime)");
    }
}
