//
//  FrameScore.swift
//  BGL
//
//  Created by MAC on 1/28/17.
//  Copyright © 2017 Zahur Ghotlawala. All rights reserved.
//

import Foundation

class FrameScore {
    enum FrameScoreType{
        case Gutter
        case Strike
        case Spare
        case Shot
    }
    var score : Int = 0;
    var scoreType = FrameScoreType.Gutter;
    var ballOneScore = 0;
    var ballSecondScore = 0
    var bonusFrame = false;
    init(_ score:Int){
        self.score = score;
    }
    init(_ type:FrameScoreType){
        self.scoreType = type;
    }
    init(_ score:Int, type:FrameScoreType) {
        self.score = score;
        self.scoreType = type
    }
    
    init(_ score:Int, type:FrameScoreType, ballOneScore :Int) {
        self.score = score;
        self.scoreType = type
        self.ballOneScore = ballOneScore;
            self.ballSecondScore = score - ballOneScore
    }
    init(_ score:Int, type:FrameScoreType, ballOneScore :Int, bonusFrame: Bool) {
        self.score = score;
        self.scoreType = type
        self.ballOneScore = ballOneScore;
        self.ballSecondScore = score - ballOneScore
        self.bonusFrame = bonusFrame;
    }
}
