//
//  ViewController.swift
//  BGL
//
//  Created by MAC on 1/28/17.
//  Copyright © 2017 Zahur Ghotlawala. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//Just uncomment the test you would like to run
//        performTest1();
        performTest2();
        
        
    }
    
    func performTest1(){
        let _ = Book(); //Defualt URL
        //let _ = Book("http://www.loyalbooks.com/download/text/Railway-Children-by-E-Nesbit.txt"); //Custom URL
        
    }
    
    func performTest2(){
        
        
        let usersFrame = ["X|X|X|X|X|X|X|X|X|X||XX",
                     "9-|9-|9-|9-|9-|9-|9-|9-|9-|9-||",
                     "5/|5/|5/|5/|5/|5/|5/|5/|5/|5/||5",
                     "X|7/|9-|X|-8|8/|-6|X|X|X||81",
                     "X|7|9-|X|-8|8/|-6|X|X|X||812",  //Invalid bonus balls
            "X|7*|9-|X|-8|8/|-6|X|X|X||81",   //Invalid character
            "X|9-|X|-8|8/|-6|X|X|X||81"   //Less frames
        ]
        
        
        for userFrame in usersFrame{
            
            let gameScoreObj = GameScore(userFrame: userFrame);
            NSLog("userFrame: \(gameScoreObj.userFrame) gameScore \(gameScoreObj.gameScore)");
        }
        
        
    }
    
    
    
}

