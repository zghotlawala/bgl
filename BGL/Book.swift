//
//  Book.swift
//  BGL
//
//  Created by MAC on 1/28/17.
//  Copyright © 2017 Zahur Ghotlawala. All rights reserved.
//

import Foundation
import Alamofire

class Book {
    let defautlBookUrl = "http://www.loyalbooks.com/download/text/Railway-Children-by-E-Nesbit.txt"
    var urlStr = "";
    init() {
        self.urlStr = defautlBookUrl;
        self.downloadFile { (isSuccessful, dicWords, error) in
            if isSuccessful{
                NSLog("------Words Found------");
                for (_, word) in dicWords!{
                    let wordObj = word as! WordDetails;
                    wordObj.checkForPrime();
                    wordObj.print();
                }
            }else{
                if let _ = error{
                    NSLog("error : \(error.debugDescription)");
                }else{
                    NSLog("error : No words found");
                }
            }
        }
    }
    
    init(_ bookUrl: String){
        self.urlStr = bookUrl;
        self.downloadFile { (isSuccessful, dicWords, error) in
            if isSuccessful{
                NSLog("------Words Found------");
                for (_, word) in dicWords!{
                    let wordObj = word as! WordDetails;
                    wordObj.checkForPrime();
                    wordObj.print();
                }
            }else{
                if let _ = error{
                    NSLog("error : \(error.debugDescription)");
                }else{
                    NSLog("error : No words found");
                }
            }
        }
        
    }
    
    
    // Downloads the predifined url and return the success or failure as output
    func downloadFile(completionBlock:((_ isSuccesfull: Bool, _ words:NSMutableDictionary?, _ error: Error?)->Void)?){
        Alamofire.request(urlStr)
            .responseString { response in
                switch response.result{
                case .success(let value):
                    let dicWords = self.convertToWords(paragraph: value)
                    if dicWords.allKeys.count > 0{
                        completionBlock!(true, dicWords, nil);
                    }
                    else{
                        completionBlock!(false, nil, nil);
                    }
                    
                case .failure(let error):
                    completionBlock!(false, nil, error);
                    break
                }
        }
    }
    
    // Convert the complete content into words, exceptions of caseinsensitive and panchautions.
    func convertToWords(paragraph:String) -> NSMutableDictionary{
        var words = NSMutableDictionary();
        let inputRange = CFRangeMake(0, paragraph.lengthOfBytes(using: .utf8))  //Complete length
        let flag = UInt(kCFStringTokenizerUnitWord)
        let locale = CFLocaleCopyCurrent() //Performs check as per the device locale
        
        let tokenizer = CFStringTokenizerCreate( kCFAllocatorDefault, paragraph as CFString!, inputRange, flag, locale)
        var tokenType = CFStringTokenizerAdvanceToNextToken(tokenizer) // loop will exit if no more words found
        
        while tokenType != CFStringTokenizerTokenType.init(rawValue: CFOptionFlags.zero)
        {
            let currentTokenRange = CFStringTokenizerGetCurrentTokenRange(tokenizer)
            //Extract the word
            let nsrange = NSMakeRange(currentTokenRange.location, currentTokenRange.length)
            let word = (paragraph as NSString).substring(with: nsrange)
            
            self.checkUniqueness(word: word, dic: &words)
            tokenType = CFStringTokenizerAdvanceToNextToken(tokenizer)
        }
        
        return words
    }
    //    Check uniqueness of the word using dictionaary keys. If exist add the word count otherwise, add the word in dictionary.
    func checkUniqueness(word:String, dic: inout NSMutableDictionary){
        if let existingWord = dic.object(forKey: word) as? WordDetails{
            existingWord.counter += 1;
        }else{
            let wordObj = WordDetails(comparator: word)
            dic.setObject(wordObj, forKey: word as NSCopying);
        }
    }
    
    
    
}

