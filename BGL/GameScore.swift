//
//  GameScore.swift
//  BGL
//
//  Created by MAC on 1/28/17.
//  Copyright © 2017 Zahur Ghotlawala. All rights reserved.
//

import Foundation


class GameScore{

    var gameScore = 0;
    var userFrame:String = "";
    
    init(userFrame:String){
        self.userFrame = userFrame;
        gameScore = self.getScoreForFrame()
    }
    
    func getScoreForFrame() -> Int{
        gameScore = 0; //reset
        var i = 1; //loopIndex
        var isBonusFrame = false;
        var normalBallFrames : String!
        var bonusBallFrames : String!
        var arrScroreFrames = [FrameScore]();
        
        let typeOfBalls = userFrame.components(separatedBy: "||")  // Split types
        if typeOfBalls.count >= 1{
            normalBallFrames = typeOfBalls[0];
            if typeOfBalls.count > 1{
                bonusBallFrames = typeOfBalls[1];
            }
        }
        
        var totalBalls = normalBallFrames.components(separatedBy: "|")  // Split types
        if totalBalls.count < 10{
            NSLog("error - Less number of frames");
            return gameScore;
        }
        
        //Bonus Balls
        let numberOfBonusBolls = bonusBallFrames.lengthOfBytes(using: .utf8)
        
        if numberOfBonusBolls == 2{
            let firstBonusBall =  String(bonusBallFrames.characters.prefix(1))
            totalBalls.append(firstBonusBall);
            let secondBonusBall =  String(bonusBallFrames.characters.suffix(1))
            totalBalls.append(secondBonusBall);
        }else if numberOfBonusBolls == 1{
            let firstBonusBall =  String(bonusBallFrames.characters.prefix(1))
            totalBalls.append(firstBonusBall);
        }else if numberOfBonusBolls != 0{
            NSLog("error - Invalid Bonus balls not found -user \(userFrame)")
            return -1;
        }
        
        
        for twoBalls in totalBalls{
            if i<10{
                isBonusFrame = false;
            }else{
                isBonusFrame = true;
            }
            let lenght = twoBalls.lengthOfBytes(using: .utf8)
            if lenght == 1{ // Only one Ball played in the frame
                let ball = String(twoBalls.characters.prefix(1))
                
                let currentFrameScore = getScooreForBall(Ball: ball, previousBall: nil)
                if currentFrameScore == 10{
                    arrScroreFrames.append(FrameScore(currentFrameScore,type:FrameScore.FrameScoreType.Strike, ballOneScore: currentFrameScore,bonusFrame:isBonusFrame));
                }else{
                    arrScroreFrames.append(FrameScore(currentFrameScore, type:FrameScore.FrameScoreType.Shot, ballOneScore: currentFrameScore,bonusFrame:isBonusFrame));
                }
                
            }else if lenght == 2{ // Two Balls played in this frame
                let firstBall = String(twoBalls.characters.prefix(1))
                let firstBallScore = getScooreForBall(Ball: firstBall, previousBall: nil);
                let secondBall = String(twoBalls.characters.suffix(1))
                let secondBallScore = getScooreForBall(Ball: secondBall, previousBall: firstBall);
                let currentFrameScore = firstBallScore + secondBallScore;
                
                if currentFrameScore == 10{
                    arrScroreFrames.append(FrameScore(currentFrameScore,type:FrameScore.FrameScoreType.Spare, ballOneScore:firstBallScore,bonusFrame:isBonusFrame));
                }else{
                    arrScroreFrames.append(FrameScore(currentFrameScore,type:FrameScore.FrameScoreType.Shot, ballOneScore:firstBallScore,bonusFrame:isBonusFrame));
                }
            }else{
                if lenght == 0{
                    NSLog("error - No Balls found in frame \(i)");
                }else{
                    NSLog("error - Incorrect number of Balls found in frame \(i)");
                }
            }
            i += 1
        }
        i = 0;
        for currentFrameScore in arrScroreFrames{
            
//            let baseScore = currentFrameScore.score;
            if currentFrameScore.scoreType == .Strike && i<10{
                
                let firstNextFrame = arrScroreFrames[i+1]
                if firstNextFrame.bonusFrame{
                    currentFrameScore.score += firstNextFrame.score
                    let secondNextBall = arrScroreFrames[i+2];
                    currentFrameScore.score += secondNextBall.score
                }else{
                    if firstNextFrame.scoreType == .Strike{
                        currentFrameScore.score += firstNextFrame.score
                        let secondNextBall = arrScroreFrames[i+2];
                        if secondNextBall.scoreType == .Strike{
                            currentFrameScore.score += secondNextBall.score
                        }else{
                            currentFrameScore.score += secondNextBall.ballOneScore
                        }
                    }else{
                        currentFrameScore.score += firstNextFrame.score
                    }
                    
                }
                gameScore += currentFrameScore.score;
            }
            else if currentFrameScore.scoreType == .Spare && i<10{
                
                let firstNextFrame = arrScroreFrames[i+1]
                if firstNextFrame.scoreType == .Strike{
                    currentFrameScore.score += firstNextFrame.score
                    
                }else{
                    currentFrameScore.score += firstNextFrame.ballOneScore
                }
                gameScore += currentFrameScore.score;
            }else if i<10{
                gameScore += currentFrameScore.score;
            }
            //NSLog("[\(i+1)] \(currentFrameScore.score)  baseScore \(baseScore)"); Uncommet to view the score for each frame
            i += 1
            
        }
        
        return gameScore;
        
    }
    
    func getScooreForBall(Ball:String, previousBall:String?) -> Int{
        var score = 0;
        if Ball.compare("X") == .orderedSame{
            score = 10
        }else if Ball.compare("/") == .orderedSame{
            score = 10 - Int(previousBall!)!
            if score > 10{
                NSLog("error - Invalid strike value found");
                score = 0;
            }
        }else if Ball.compare("-") == .orderedSame{
            score = 0
        }else {
            if let intValue = Int(Ball){
                score = intValue
            }else{
                NSLog("error - Invalid ball input found \(Ball)");
            }
        }
        return score;
    }


}
